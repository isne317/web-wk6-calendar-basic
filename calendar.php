<style>

.calendar
{
	position:relative;
	width: 960px;
}
div.date, div.days
{
	width: 120px;
	float: left;
	margin: 1px;
	border: 1px solid black;
}
.blankday
{
	background:#ccc;
}
div.date
{
	height: 120px;
}
.today
{
	background:#cfc;   
}
input[type=submit]
{
	border: none;
	cursor: pointer;
	background-color: transparent;
	font-size: 1.1em;
}

</style>

<?php

echo "<h2>Calendar of " . date('F, Y',strtotime($_POST["date"])) . "</h2>";

?>

<div class="calendar">
	<div class="days">Sunday</div>
	<div class="days">Monday</div>
	<div class="days">Tuesday</div>
	<div class="days">Wednesday</div>
	<div class="days">Thursday</div>
	<div class="days">Friday</div>
	<div class="days">Saturday</div>
	
<?php
$day = date('d', strtotime($_POST['date']));
$month = date('m', strtotime($_POST['date']));
$year = date('Y', strtotime($_POST['date']));
$firstday = date('w', strtotime('01-' . $month . '-' . $year));
$days = date('t', strtotime($_POST['date']));
$title = $_POST['title'];
$desc = $_POST['desc'];
$fdate = $_POST['date'];
$today = date('d');
$todaymonth = date('m');
$todayyear = date('Y');

for($i=1; $i<=$firstday; $i++)
{
	echo '<div class="date blankday"></div>';
}
for($i=1; $i<=$days; $i++)
{
	echo '<div class="date';
	if ($today == $i && $todaymonth==$month && $todayyear == $year)
	{
		echo ' today';
	}
	echo '">' . $i . '<br>';
	if($day==$i)
	{
		echo "<form id='day' action='info.php' method='POST'>";
		echo "<input type='submit' value=" . $title . ">";
		echo "<input name='title' type='hidden' value=" . $title . ">";
		echo "<input name='desc' type='hidden' value=" . $desc . ">";
		echo "<input name='date' type='hidden' value=" . $fdate . ">";
		echo "</form>";
	}
	echo '</div>';
}
$daysleft = 7-(($days + $firstday)%7);
if($daysleft<7)
{
	for($i=1; $i<=$daysleft; $i++)
	{
		echo '<div class="date blankday"></div>';
	}
}
?>