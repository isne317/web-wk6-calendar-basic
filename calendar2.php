<html>
<style>

table
{
	font-family: arial, sans-serif;
	width: 100%;
	border: 1px solid black;
	border-collapse: collapse;
}
th, td
{
	border: 1px solid black;
	padding: 4px 8px;
}

tr:nth-child(even)
{
	background-color: #dddddd;
	font-size: 0.75em;
	text-align: right;
}

tr:nth-child(odd)
{
	font-size: 1.2em;
	height: 50;
	word-wrap: normal;
	font-weight: bold;
}

</style>


<body>

<p id="test"></p>

</body>
</html>

<?php

$rot = 0;
$month = date('m',strtotime($_POST["date"]));
$year = date('Y',strtotime($_POST["date"]));
$dowMap = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
$dateShort = array('SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT');
$dateListed = array();

echo "<h2>Calendar of ";
echo date('F, Y', strtotime('01-'.$month.'-'.$year)) . "<br>";
echo "</h2>";

echo "<table><tr>";

//Display day names
foreach($dateShort as $i)
{
	echo "<th>" . $i . "</th>";
}
echo "</tr>";

$dateListed = getDateListed($month, $year, $dowMap);

$rot = 0;
$out = false;
while($rot < 42 && !$out)
{
	echo "<td>" . $dateListed[$rot] . "</td>";
	if($rot % 7 == 6)
	{
		echo "</tr>";
		
		$rot-=6;
		for($i = 1; $i <= 7; $i++)
		{
			if(strtotime($dateListed[$rot] . '-' . $month . '-' . $year) == strtotime($_POST["date"]))
			{
				echo "<td><p onclick='test()'>" . $_POST["title"] . "</p></td>";
			}
			else
			{
				echo "<td> </td>";
			}
			$rot++;
		}
		$rot--;
		if($dateListed[$rot] != " ")
		{
			echo "<tr>";
		}
		else
		{
			$out = true;
		}
	}
	$rot++;
}
echo "</tr></table>";

/* Fn Zone */

function getDateListed($month, $year, $dowMap)
{
	/*
		Get date number corresponding to day names in the form of array.
	*/
	$rot = 0;
	$try = -1;
	$dateListed = array();
	while($rot < 42)
	{
		$try = ($rot+1) - array_search(date('l', strtotime('01-'.	$month.'-'.$year)), $dowMap);
		if((date('m', strtotime($try . '-' . $month . '-' . $year)) == date('m', strtotime('01-' . $month . '-' . $year))))
		{
			$dateListed[$rot] = $try;
		}
		else
		{
			$dateListed[$rot] = ' ';
		}
		$rot++;
	}
	return $dateListed;
}

?>

<script>

function test()
{
	document.getElementById("test").innerHTML = "Hi";
}

</script>