<html>
<head>
	<title>wk6</title>
	
<style>

div
{
	background-color: #dddddd;
	padding: 16px 8px;
	border-radius: 10px;
}

input[type=text]
{
	width: 100%;
	margin: 8px 0;
	font-size: 1em;
	padding: 8px 4px;
}

input[type=date]
{
	width: 100%;
	margin: 8px 0;
	font-size: 1em;
	padding: 8px 4px;
}

input[type=submit]
{
	width: 100%;
	background-color: #2ECC40;
	font-size: 1.1em;
	color: #ffffff;
	border: none;
	border-radius: 4px;
	padding: 8px 4px;
	cursor: pointer;
	margin: 8px 0;
	font-weight: bold;
}

input[type=reset]
{
	width: 100%;
	background-color: #FF4136;
	font-size: 1.1em;
	color: #ffffff;
	border: none;
	border-radius: 4px;
	padding: 8px 4px;
	cursor: pointer;
	margin: 8px 0;
	font-weight: bold;
}

</style>
</head>

<body>

<h1>Let's make an appointment!</h1>
<div>
<form action="calendar.php" method="post">

<input type="date" name="date">
<input type="text" name="title" placeholder="Title (Max 40 characters)">
<input type="text" name="desc" placeholder="Descriptions">
<input type="submit" value="SEND">
<input type="reset" value="RESET">

</form>
</div>
</body>

</html>